const VRack = require('vrack-core')
const Port = VRack.Port

module.exports = class extends VRack.Device {
  ports () {
    return [
      new Port('entity').input().data('mixed').description('Сущность для хранения'),
      new Port('shift').input().data('number').description('Количество удаляемых строк из массива'),
      new Port('slice').input().data('number').description('Количество получаемых строк из массива'),

      new Port('entities').output().data('Array').description('Массив значений в количестве переданного числа **slice**')
    ]
  }

  settings () {
    return { message: false }
  }

  shares = { bufferCount: 0 };

  buffer = [];

  process () {
    setTimeout(() => {
      this.render()
    }, 1000)
  }

  inputSlice (count) {
    if (this.buffer.length < count) count = this.buffer.length
    this.outputs.entities.push(this.buffer.slice(0, count))
  }

  inputEntity (data) {
    this.buffer.push(data)
    this.updateBufferCount()
  }

  inputShift (count) {
    this.buffer.splice(0, count)
    this.updateBufferCount()
  }

  updateBufferCount () {
    this.shares.bufferCount = this.buffer.length
    this.render()
  }
}
