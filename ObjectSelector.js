const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Получает объект и отравляет отдельные его значения на выходы'

  ports () {
    return [
      new Port('object').input().data('mixed').description('Объект из которого будут извлекатся занчения'),
      new Port('unit%d').output().data('mixed').dynamic(this.params.select.length).description('Значение')
    ]
  }

  checkParams () {
    return [
      new Rule('select').default([]).isArray()
        .example(['event', 'card'])
        .description('Массив путей значений в формате `path.to.value`')
    ]
  }

  settings () {
    return { message: false, shares: false }
  }

  inputObject (data) {
    var i = 1
    for (const select of this.params.select) {
      this.outputs['unit' + i].push(this.getFromPath(select, data))
      i++
    }
  }

  getFromPath (acts, data) {
    acts = acts.split('.')
    var sdata = data
    var i = 1
    for (var act of acts) {
      if (sdata === undefined) return sdata
      if (acts.length === i) { return sdata[act] } else { sdata = sdata[act] } i++
    }
  }
}
