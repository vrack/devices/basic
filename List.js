
const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Преобразует одно значение в другое (аналог ассоциативного массива)'
  ports () {
    return [
      new Port('index').input().data('number').description('Значение для вычисления'),
      new Port('gate').input().data('signal').description('Сигнальный вход'),

      new Port('value').output().data('mixed').description('Идентификатор канала'),
      new Port('gate').output().data('signal').description('Сигнал завершения передачи')
    ]
  }

  checkParams () {
    return [
      new Rule('list').default({ 1: 'test' }).isObject()
        .description('Ассоциативный объект')
        .example({
          1: '/metric/memory/max',
          2: '/metric/memory/avg',
          3: '/metric/memory/min'
        })
    ]
  }

  settings () {
    return {
      message: false,
      shares: false,
      storage: false
    }
  }

  unit = 0;

  inputIndex (data) {
    this.unit = data
    if (!this.inputs.gate.connected) this.outGate(1)
  }

  inputGate (data) {
    this.outGate(data)
  }

  outGate (data) {
    this.outputs.value.push(this.params.list[this.unit])
    this.outputs.gate.push(data)
  }
}
