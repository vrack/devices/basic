const { Rule, Port, Device, DeviceError } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('mix%d').input().data('mixed').dynamic(this.params.inputs).description('Значение'),
      new Port('fx%d.mix').input().data('mixed').dynamic(this.params.inputsFX).description('Значение FX группы'),
      new Port('fx%d.channel').input().data('mixed').dynamic(this.params.inputsFX).description('Номер канала FX группы'),
      new Port('fx%d.gate').input().data('signal').dynamic(this.params.inputsFX).description('Сигнальный вход FX группы'),

      new Port('mixed').output().data('mixed').description('Значение основного выхода'),
      new Port('channel').output().data('mixed').description('Номер канала'),
      new Port('gate').output().data('signal').description('Сигнальный выход'),

      new Port('fx%d.mix').output().data('mixed').dynamic(this.params.outputsFX).description('Значение FX группы'),
      new Port('fx%d.channel').output().data('mixed').dynamic(this.params.outputsFX).description('Номер канала FX группы'),
      new Port('fx%d.gate').output().data('signal').dynamic(this.params.outputsFX).description('Сигнальный вход FX группы')
    ]
  }

  checkParams () {
    return [
      new Rule('inputs').default(0).isInteger().expression('value >= 0').example(8)
        .description('Количество обычных входов'),
      new Rule('inputsFX').default(0).isInteger().expression('value >= 0').example(8)
        .description('Количество `FX` входов'),
      new Rule('outputsFX').default(0).isInteger().expression('value >= 0').example(8)
        .description('Количество `FX` выходов'),
      new Rule('routes').default({}).isObject().example({ 2: 'fx1' })
        .description('Настройка маршрутизации')
    ]
  }

  settings () {
    return { message: false, shares: false }
  }

  FXUnits = {}
  FXChannels = {}

  preProcess () {
    // add mix inputs
    for (let i = 1; i <= this.params.inputs; i++) {
      this['inputMix' + (i)] = data => {
        if (this.params.routes[i]) {
          if (typeof this.params.routes[i] === 'string') {
            this.outFX(this.params.routes[i], i, data)
          }
          if (Array.isArray(this.params.routes[i])) {
            for (const out of this.params.routes[i]) {
              if (out === 'main') this.outGate(i, data)
              else this.outFX(out, i, data)
            }
          }
        } else {
          this.outGate(i, data)
        }
      }
    }
    // add fx inputs
    for (let i = 1; i <= this.params.inputsFX; i++) {
      this[`inputFx${i}Mixed`] = data => { this.FXUnits[i] = data }
      this[`inputFx${i}Channel`] = data => { this.FXChannels[i] = data }
      this[`inputFx${i}Gate`] = data => { this.outGate(this.FXChannels[i], this.FXUnits[i], data) }
    }
  }

  process () {
    for (const index in this.params.routes) {
      if (
        !this.inputs[`mix${index}`] ||
        (
          !this.outputs[`${this.params.routes[index]}.mixed`] &&
          this.params.routes[index] !== 'main'
        )
      ) this.terminate(new DeviceError(`Invalid route (${index} => ${this.params.routes[index]})`), 'init')
    }
  }

  outFX (fx, channel, data) {
    this.outputs[`${fx}.mixed`].push(data)
    this.outputs[`${fx}.channel`].push(Number(channel))
    this.outputs[`${fx}.gate`].push(1)
  }

  outGate (channel, data, gate) {
    this.outputs.mixed.push(data)
    this.outputs.channel.push(Number(channel))
    this.outputs.gate.push(gate)
  }
}
