const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Получает объект и отравляет отдельные его значения на выходы в виде массива'

  ports () {
    return [
      new Port('object').input().data('mixed').description('Объект из которого будут извлекатся занчения'),
      new Port('array').output().data('array').description('Массив значений')
    ]
  }

  checkParams () {
    return [
      new Rule('select').required().default([]).isArray()
        .example(['event', 'card'])
        .description('Массив путей значений в формате `path.to.value`')
    ]
  }

  settings () {
    return { message: false, shares: false }
  }

  inputObject (data) {
    const resultArray = []
    for (const select of this.params.select) resultArray.push(this.getFromPath(select, data))
    this.outputs.array.push(resultArray)
  }

  getFromPath (acts, data) {
    acts = acts.split('.')
    var sdata = data
    var i = 1
    for (var act of acts) {
      if (sdata === undefined) return sdata
      if (acts.length === i) { return sdata[act] } else { sdata = sdata[act] } i++
    }
  }
}
