
const VRack = require('vrack-core')
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Формирует разницу текущего значения и предыдущего';

  ports () {
    return [
      new Port('unit').input().data('number').description('Значение для вычисления'),
      new Port('channel').input().data('mixed').description('Идентификатор канала (уникально для аггрегации)'),
      new Port('gate').input().data('signal').description('Сигнальный вход'),

      new Port('diff').output().data('number').description('Разница между предидущим значением'),
      new Port('channel').output().data('mixed').description('Идентификатор канала'),
      new Port('gate').output().data('signal').description('Сигнал завершения передачи')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  channel = 1;
  unit = 0;
  values = {};

  inputUnit (data) {
    this.unit = Number(data)
    if (!this.inputs.gate.connected) this.inputGate(1)
  }

  inputChannel (data) {
    this.channel = data
  }

  inputGate (data) {
    if (data === undefined) data = 1
    this.outGate(data)
  }

  outGate (data) {
    if (this.values[this.channel] === undefined) {
      this.values[this.channel] = this.unit
      return
    }

    var old = this.values[this.channel]
    this.values[this.channel] = this.unit
    this.outputs.diff.push(this.unit - old)
    this.outputs.channel.push(this.channel)
    this.outputs.gate.push(data)
  }
}
