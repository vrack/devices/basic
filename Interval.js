
const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Формирует сигнал через заданное время'
  ports () {
    return [
      new Port('start').input().data('signal').description('Значение'),
      new Port('stop').input().data('signal').description('Идентификатор канала'),
      new Port('gate').output().data('signal').description('Сигнальный выход')
    ]
  }

  checkParams () {
    return [
      new Rule('timeout').required().default(1000).isInteger().expression('value > 0')
        .description('Интервал в ms'),
      new Rule('start').required().default(true).isBoolean()
        .description('Начинает работать сразу после запуска')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  timer = false;

  process () {
    if (this.params.start === true) this.inputStart()
  }

  startTimer () {
    if (this.timer === false) {
      this.timer = setInterval(() => {
        this.outputs.gate.push(1)
      }, this.params.timeout)
    }
  }

  inputStart (data, input, output) {
    this.startTimer()
  }

  inputStop () {
    if (this.timer !== false) {
      clearInterval(this.timer)
      this.timer = false
    }
  }
}
