const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  ports () {
    return [
      new Port('gate%d').input().data('signal').dynamic(this.params.timers.length)
        .description('Сигнальный вход'),
      new Port('gate%d').output().data('signal').dynamic(this.params.timers.length)
        .description('Сигнальный выход')
    ]
  }

  checkParams () {
    return [
      new Rule('timers')
        .default([{ timeout: 1000, relative: true }])
        .isArray()
        .custom('Ivalid timers format', (value) => {
          for (const key in value) {
            if (
              value[key].timeout === undefined ||
              value[key].relative === undefined
            ) return false
          }
          return true
        })
        .description('Массив таймеров')
    ]
  }

  settings () {
    return { message: false }
  }

  shares = {
    timers: {}
  };

  preProcess () {
    for (let port = 1, index = 0; port <= this.params.timers.length; port++, index++) {
      this.shares.timers[port] = { timeout: false, gate: false }
      this['inputGate' + port] = (data) => {
        this.inputGate(data, port, index)
      }
      this.gates[port] = null
    }
  }

  timers = {};
  gates = {};

  process () {
    for (var port = 1, index = 0; port <= this.params.timers.length; port++, index++) {
      if (this.params.timers[index].relative) this.startTimer(port, index)
    }
  }

  inputGate (data, port, index) {
    this.shares.timers[port].gate = true
    this.gates[port] = data
    if (this.timers[port]) return
    if (this.params.timers[index].relative) {
      if (this.shares.timers[port].timeout) this.outGate(port, index)
    } else {
      this.startTimer(port, index)
    }
    this.render()
  }

  startTimer (port, index) {
    this.timers[port] = setTimeout(() => {
      this.shares.timers[port].timeout = true
      if (this.params.timers[index].relative) {
        if (this.shares.timers[port].gate) this.outGate(port, index)
      } else {
        this.outGate(port, index)
      }
      this.timers[port] = false
      this.render()
    }, this.params.timers[index].timeout)
  }

  outGate (port, index) {
    this.shares.timers[port].timeout = false
    this.shares.timers[port].gate = false
    this.outputs['gate' + port].push(this.gates[port])
    if (this.params.timers[index].relative) this.startTimer(port, index)
    this.render()
  }
}
