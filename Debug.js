
const VRack = require('vrack-core')
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Отправляет сообещния в канал `terminal` все приходящие данные'
  ports () {
    return [
      new Port('debug%d').input().data('mixed').dynamic(8)
        .description('Вход отладочной информации')
    ]
  }

  settings () {
    return {
      shares: false,
      storage: false,
      messageTypes: ['terminal']
    }
  }

  preProcess () {
    for (let i = 1; i <= 8; i++) {
      this['inputDebug' + i] = (data) => { this.toDebug(data, i) }
    }
  }

  toDebug (data, input) {
    this.terminal('Port debug' + input, data)
  }
}
