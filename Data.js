const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port

module.exports = class extends VRack.Device {
  description = 'Последовательно отправляет заранее подготовленную данные через заданные интервалы времени.'

  ports () {
    return [
      new Port('start').input().data('signal').description('Получает сигнал для начала / продолжения работы'),
      new Port('stop').input().data('signal').description('Получает сигнал для остановки'),
      new Port('reset').input().data('signal').description('Получает сигнал сброса состояния работы - начинает работать с начала'),
      new Port('unit').output().data('mixed').description('Данные для отправки')
    ]
  }

  checkParams () {
    return [
      new Rule('data')
        .required()
        .isArray()
        .expression('value.length > 0')
        .custom('Invalid data format (value, timeout)', () => {
          for (const data of this.params.data) if (!data.timeout || data.value === undefined) return false
          return true
        }).example([
          { value: 1, timeout: 1000 },
          { value: 2, timeout: 2000 },
          { value: 3, timeout: 1000 }
        ])
        .description('Данные `value` массива'),
      new Rule('autoStart').default(true).required().isBoolean()
        .description('Начинает работать сразу после запуска'),
      new Rule('doOnEnd').default('reset').enum(['reset', 'nothing']).isString()
        .description('Что делать после достижения конца массива `data` (`reset`,`nothing`)')
    ]
  }

  settings () {
    return { message: false }
  }

  shares = {
    start: false,
    index: 0
  }

  queueTimer = false

  process () {
    if (this.params.autoStart) this.inputStart()
    setInterval(this.render.bind(this), 3000)
  }

  inputStart () {
    if (this.shares.start) return
    this.shares.start = true
    this.run()
  }

  inputStop () {
    if (!this.shares.start) return
    if (this.queueTimer) {
      clearTimeout(this.queueTimer)
      this.queueTimer = false
    }
  }

  inputReset () {
    if (this.queueTimer) {
      clearTimeout(this.queueTimer)
      this.queueTimer = false
    }
    this.shares.index = 0
    this.inputStart()
  }

  queueNext () {
    this.shares.index++
    if (this.shares.index === this.params.data.length) {
      this.shares.index = 0
      if (this.params.doOnEnd === 'nothing') {
        this.queueTimer = false
        this.shares.start = false
        this.render()
        return
      }
    }
    this.run()
    this.render()
  }

  run () {
    const data = this.params.data[this.shares.index]
    this.queueTimer = setTimeout(() => {
      this.outputs.unit.push(data.value)
      this.queueNext()
    }, data.timeout)
  }
}
