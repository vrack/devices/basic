const VRack = require('vrack-core')
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Формирует сигнал на выходе если новый входной сигнал отличается от старого'

  ports () {
    return [
      new Port('unit').input().data('mixed').description('Значение'),
      new Port('channel').input().data('mixed').description('Идентификатор канала'),
      new Port('gate').input().data('signal').description('Сигнальный вход'),

      new Port('unit').output().data('mixed').description('Значение'),
      new Port('channel').output().data('mixed').description('Идентификатор канала'),
      new Port('gate').output().data('signal').description('Сигнальный выход')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false,
      storage: false
    }
  }

  channel = 1;
  unit = 0;
  state = {};

  inputUnit (data) {
    this.unit = data
    if (!this.inputs.gate.connected) this.inputGate(1)
  }

  inputChannel (data) {
    this.channel = data
  }

  inputGate (data) {
    this.outGate(data)
  }

  outGate (data) {
    if (this.state[this.channel] === undefined) this.state[this.channel] = this.unit
    var old = this.state[this.channel]
    if (old === this.unit) return
    this.state[this.channel] = this.unit
    this.outputs.unit.push(this.state[this.channel])
    this.outputs.channel.push(this.channel)
    this.outputs.gate.push(data)
  }
}
