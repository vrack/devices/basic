const { Port, Rule } = require('vrack-core')
const VRack = require('vrack-core')
module.exports = class extends VRack.Device {
  description = 'Последовательный переключатель'

  ports () {
    return [
      new Port('gate').input().data('signal').required()
        .description('Входной сигнал переключателя'),
      new Port('start').input().data('signal')
        .description('Возобновляет работу переключатель'),
      new Port('stop').input().data('signal')
        .description('Останавливает переключатель'),
      new Port('reset').input().data('signal')
        .description('Сбрасывает счет переключателя'),
      new Port('gate%d').output().data('signal').dynamic(this.params.outputs)
        .description('Выход переключателя')

    ]
  }

  checkParams () {
    return [
      new Rule('doOnEnd').default('reset').enum(['reset', 'nothing', 'reverse']).isString()
        .description('Что делать после завершение очереди - ресет, ничего, идти в обратную сторону'),
      new Rule('outputs').default(16).isInteger().expression('value >= 0')
        .description('Количество выходов переключателя'),
      new Rule('start').default(true).isBoolean()
        .description('Включает работу по умолчанию (если `false` переключений не будет пока не получит сигнал на вход `start`)'),
      new Rule('resetSignal').default(true).isBoolean()
        .description('Если `false` - сигнал на входе `reset` не формирует сигнал на выходе после сброса')
    ]
  }

  settings () {
    return { message: false, storage: false }
  }

  shares = {
    index: 1,
    action: false,
    reverse: false
  }

  process () {
    if (this.params.start) this.shares.action = true
    setInterval(() => {
      this.render()
    }, 5000)
  }

  incrementIndex () {
    if (this.shares.reverse) {
      if (this.shares.index === 1) {
        this.shares.reverse = false
        this.shares.index++
      } else this.shares.index--
    } else {
      if (this.shares.index === this.params.outputs) {
        switch (this.params.doOnEnd) {
          case 'reset':
            this.shares.index = 1
            break
          case 'nothing':
            break
          case 'reverse':
            this.shares.reverse = true
            this.shares.index--
            break
          default:
            this.shares.index = 1
        }
      } else {
        this.shares.index++
      }
    }
  }

  inputGate (data, input, output) {
    if (this.shares.action) {
      this.incrementIndex()
      this.outGate(data)
    }
  }

  inputStop () {
    this.shares.action = false
    this.render()
  }

  inputStart () {
    this.shares.action = true
    this.render()
  }

  inputReset () {
    this.shares.index = 1
    this.shares.reverse = false
    if (this.params.resetSignal) { this.outGate() } else { this.render() }
  }

  outGate (data) {
    this.outputs['gate' + this.shares.index].push(data)
    this.render()
  }
}
