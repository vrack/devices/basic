const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Формирует случайное число  (многоканальный) (под вопросом)'

  ports () {
    return [
      new Port('gate').input().data('signal')
        .description('Сигнал формирования значений'),
      new Port('unit%d').output().data('number').dynamic(this.params.outputs.length)
        .description('Значение'),
      new Port('gate').output().data('signal')
        .description('Сигнальный выход')
    ]
  }

  checkParams () {
    return [
      new Rule('outputs')
        .default([{ max: 10, min: 1, dvd: 1 }])
        .isArray()
        .custom('Invalid outputs param', (value) => {
          for (const key in value) {
            if (typeof value[key].max !== 'number' ||
                typeof value[key].min !== 'number' ||
                typeof value[key].dvd !== 'number'
            ) return false
          }
          return true
        })
        .description('Массив настроек выходов')
    ]
  }

  settings () {
    return { message: false, shares: false }
  }

  inputGate (data) {
    this.outGate(data)
  }

  outGate (data) {
    var i = 1
    for (var key in this.params.outputs) {
      var conf = this.params.outputs[key]
      this.outputs['unit' + i].push((Math.floor(conf.min + Math.random() * (conf.max - conf.min + 1))) / conf.dvd)
      i++
    }
    this.outputs.gate.push(data)
  }
}
