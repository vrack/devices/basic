const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Формирует максимальное, минимальное, среднее значение на основе буфера семплов';

  ports () {
    return [
      new Port('unit').input().data('number').description('Значение для вычисления'),
      new Port('channel').input().data('mixed').description('Идентификатор канала (уникально для аггрегации)'),
      new Port('gate').input().data('signal').description('Сигнальный вход'),

      new Port('max').output().data('number').description('Максимальное значение'),
      new Port('avg').output().data('number').description('Среднее значение'),
      new Port('min').output().data('number').description('Минимальное значение'),
      new Port('channel').output().data('mixed').description('Идентификатор канала'),
      new Port('gate').output().data('signal').description('Сигнал завершения передачи')
    ]
  }

  checkParams () {
    return [
      new Rule('samples').default(32).isInteger().expression('value > 0')
        .description('Количество семплов'),
      new Rule('cyclicBuffer').default(false).isBoolean()
        .description('true - записывает значения в буфер по кругу, false - ждет накопления буфера')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  buffer = {};
  index = {};
  channel = 1;
  unit = 0;

  inputUnit (data) {
    this.unit = data
    if (!this.inputs.gate.connected) this.inputGate(1)
  }

  inputChannel (data) {
    this.channel = data
  }

  inputGate (data) {
    if (data === undefined) data = 1
    if (this.buffer[this.channel] === undefined) this.buffer[this.channel] = []
    if (this.params.cyclicBuffer) {
      this.buffer[this.channel].push(this.unit)
      if (this.buffer[this.channel].length >= this.params.samples) this.buffer[this.channel].shift()
      this.outGate(data)
    } else {
      if (this.index[this.channel]) { this.index[this.channel]++ } else { this.index[this.channel] = 1 }
      this.buffer[this.channel].push(this.unit)
      if (this.index[this.channel] >= this.params.samples) {
        this.index[this.channel] = 1
        this.outGate(data)
        this.buffer[this.channel] = []
      }
    }
  }

  outGate (data) {
    if (this.outputs.max.connected) {
      var max = Math.max.apply(null, this.buffer[this.channel])
      this.outputs.max.push(max)
    }
    if (this.outputs.min.connected) {
      var min = Math.min.apply(null, this.buffer[this.channel])
      this.outputs.min.push(min)
    }
    if (this.outputs.avg.connected) {
      var sum = 0
      for (var key in this.buffer[this.channel]) {
        sum += +this.buffer[this.channel][key]
      }
      var avg = sum / this.buffer[this.channel].length
      this.outputs.avg.push(avg)
    }
    this.outputs.channel.push(this.channel)
    this.outputs.gate.push(data)
  }
}
