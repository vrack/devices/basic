
const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  description = 'Фильтрует данные по разрешенному каналу или исключает все запрещенные каналы'
  ports () {
    return [
      new Port('unit').input().data('mixed').description('Значение'),
      new Port('channel').input().data('mixed').description('Идентификатор канала'),
      new Port('gate').input().data('signal').description('Сигнальный вход'),

      new Port('unit').output().data('mixed').description('Значение'),
      new Port('channel').output().data('mixed').description('Идентификатор канала'),
      new Port('gate').output().data('signal').description('Сигнальный выход')
    ]
  }

  checkParams () {
    return [
      new Rule('include').required().default([]).isArray().example([1, 2, 3, 4, 5])
        .description('Список разрешенных каналов'),
      new Rule('except').required().default([]).isArray()
        .description('Список запрещенных каналов')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  unit = 0;

  inputUnit (data) {
    this.unit = data
  }

  inputChannel (data) {
    this.channel = data
  }

  inputGate (data) {
    this.outGate(data)
  }

  outGate (data) {
    if ((this.params.include.length && this.params.include.indexOf(this.channel) === -1) ||
        (this.params.except.length && this.params.except.indexOf(this.channel) !== -1)
    ) return
    this.outputs.unit.push(this.unit)
    this.outputs.channel.push(this.channel)
    this.outputs.gate.push(data)
  }
}
