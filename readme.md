# basic

Базовый набор устройств для vrack

# Установка

Заходим в папку с устройствами

``` 
cd /opt/vrack/devices/
```

Клонируем репозиторий

```
git clone https://gitlab.com/vrack/basic.git
```

Не имеет зависимостей

 1. [Общее поведение](#общее-поведение)
    1. [Основные правила](#основные-правила)
    2. [Типы входов/выходов](#типы-входоввыходов)
    3. [Правила входов/выходов](#правила-входоввыходов)

Общее поведение
===============

Базовый набор содержит простые устройства которые могут помочь с построением простых схем. Данный набор пропагандирует основные рекомендации создания собственных устройств для VRack.

## Основные рекомендации

 1. Устройство должно быть комактным по возможности простым и понятным и решать конкретную задачу.
 2. Соблюдение баланса между сложностью и количеством устройств. Если устройство будет одно, но сложное, с ним тяжело будет работать. Eсли устройств будет очень много, с ними так же будет сложно работать. Отсюда следует, что вместо создания нового устройства можно доработать готовое устройство, по возможности не нарушая пункт 1.
 3. Устройство должно быть переиспользуемым. Определенную логику можно вынести в отдельное устройство, которое могут переиспользовать другие устройства, по возможности не нарушая пункт 1 и 2.

## Типы данных портов устройств

Что бы соединить два устройства, необязательно, что бы типы данных соответсвующих портов были одинаковы. Базовый набор устройств рекомендует следующие типы данных:

 - **mixed** - Любой типа данных, обычно используется в микшерах, когда типа входящих данных не имеет значения
 - **number** - обычно имеет тип данных числа, может быть с плавающей точкой
 - **signal** - тип данных не имеет значение, обычно используется для передачи факта сигнала, по умолчанию обычно равняется (1)

Основная проблема передачи объекта по связи - прием таких данных нельзя стандартизировать в широком спектре. 

Например, наше устройство шлет объект с иформацией формата, где `channel` условный номер канала а `value` его значение

```js
{
    channel: 10,
    value: 2
}
```

В таком формате нет ничего плохого. Но, к сожалению, мы не можем простым способом отделить значение одного канала от другого.

Отсюда следует вывод, что нужно по возможности разделить выходы на отдельные, для каждого канала.

Вот как мы представим наше устройство

```mermaid
graph LR;
    subgraph "Device"
        subgraph "OUTPUTS"
        channel1
        channel2
        channel3
        channel4
        channel...
        end
    end
```

Если выходов много, становится неудобно с этими выходами работать. Для агрегации выходов нам может помочь микшер каналов.

Когда на вход микшера приходят данные, он генерирует на выходе последовательно 3 сигнала: 

* **mixed** *mixed* - Значение которое поступило на вход (тип значения любой)
* **channel** *number* - Номер канала
* **gate** *signal* - Завершение передачи

Тип **signal** не определяет конкретного типа значения, сигнал для устройства, это условный сигнал для начала выполнения работы, обычно его определяют, как порт `gate`. По умолчанию значение для канала `gate` - `1` (числовая единица).

В базовом наборе есть микшер `basic.Mixer` с неограниченым набором входов

```mermaid
graph LR;
    subgraph "$1_Device"[Device1]
        subgraph "$1_OUTPUTS"[OUTPUTS]
            $1_channel1[channel1]
            $1_channel2[channel2]
            $1_channel3[channel3]
            $1_channel4[channel4]
        end
    end
    subgraph "$2_Mixer"[Mixer1]
        subgraph "$2_INPUTS"[INPUTS]
            $2_mix1[mix1]
            $2_mix2[mix2]
            $2_mix3[mix3]
            $2_mix4[mix4]
        end
        subgraph "$2_OUTPUTS"[OUTPUTS]
            $2_mixed[mixed]
            $2_channel[channel]
            $2_gate[gate]
        end
    end
    subgraph "$3_MetricDevice"[MetricDevice1]
        subgraph "$3_OUTPUTS"[INPUTS]
        $3_unit[unit]
        $3_metric[metric]
        $3_mgate[gate]
        end
    end

    $1_channel1-->$2_mix1
    $1_channel2-->$2_mix2
    $1_channel3-->$2_mix3
    $1_channel4-->$2_mix4

    $2_mixed --> $3_unit
    $2_channel --> $3_metric
    $2_gate --> $3_mgate
```

Теперь мы можем отделять данные друг от друга и модифицировать их перед отправкой в общий поток.

Например, мы можем модифицировать только занчение каналов используя `basic.List`.

Устройство `basic.List` работает как ассоциативный массив, принимая на входе значение и подменяя его на выходе:

```mermaid
graph LR;
    subgraph "$1_Device"[Device1]
        subgraph "$1_OUTPUTS"[OUTPUTS]
            $1_channel1[channel1]
            $1_channel2[channel2]
            $1_channel3[channel3]
            $1_channel4[channel4]
        end
    end
    subgraph "$2_Mixer"[Mixer1]
        subgraph "$2_INPUTS"[INPUTS]
            $2_mix1[mix1]
            $2_mix2[mix2]
            $2_mix3[mix3]
            $2_mix4[mix4]
        end
        subgraph "$2_OUTPUTS"[OUTPUTS]
            $2_mixed[mixed]
            $2_channel[channel]
            $2_gate[gate]
        end
    end
    subgraph "$3_MetricDevice"[MetricDevice1]
        subgraph "$3_INPUTS"[INPUTS]
        $3_unit[unit]
        $3_metric[metric]
        $3_mgate[gate]
        end
    end
    subgraph "$4_List1"[List1]
        subgraph "$4_INPUTS"[INPUTS]
            $4_index[index]
            $4_gate[gate]
        end
        subgraph "$4_OUTPUTS"[OUTPUTS]
            $4_value[value]
            $4_mgate[gate]
        end
    end
    $1_channel1-->$2_mix1
    $1_channel2-->$2_mix2
    $1_channel3-->$2_mix3
    $1_channel4-->$2_mix4

    $2_mixed --> $3_unit

    $2_channel --> $4_index
    $2_gate --> $4_gate

    $4_value --> $3_metric
    $4_mgate --> $3_mgate
```

## Правила входов/выходов

Если устройство использует несколько выходов для передачи данных, которые регистрируются с помощью входа `gate`, оно **обязано** передавать их последовательно, не используюя асинхронные методы или задежки между передачами, обязательно заканчивая отправкой сигнала `gate`.

```js
// выше идет подготовка данных
this.outputs.value.push(value)
this.outputs.channel.push(channel)
this.outputs.gate.push(this.gate)
```

### Правила `gate`

Порт `gate` или сигнальный порт, имеет определенные требования, что бы все устройста имеющие этот порт работали предсказуемо

 1. Устройство, которое имеет `gate` на входе, обязано иметь его и на выходе
 2. Вход `gate` всегда передает на выход значение приходящего `gate`
 3. Если устройство может обработать данные одного канала, `gate` для нее становится не обязательным. Как пример можно привести [Aggregate](#aggregate) - если `gate` не подключен, то `gate` и `channel` не будут учитываться и все приходящие данные будут считаться, как данные единственного канала (по умолчанию `1` для `gate` и `channel` ).
